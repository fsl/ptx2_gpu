#!/bin/bash

export FSLDEVDIR=$(pwd)
. $FSLDIR/etc/fslconf/fsl.sh
FSLCONFDIR=$FSLDIR/config
export FSLCONFDIR=$FSLDIR/config
export FSLMACHTYPE=`$FSLDIR/etc/fslconf/fslmachtype.sh`
export COMPILE_GPU=1

SM_20="-gencode arch=compute_20,code=sm_20"
SM_21="-gencode arch=compute_20,code=sm_21"
SM_30="-gencode arch=compute_30,code=sm_30"
SM_35="-gencode arch=compute_35,code=sm_35"
SM_37="-gencode arch=compute_37,code=sm_37"
SM_50="-gencode arch=compute_50,code=sm_50"
SM_52="-gencode arch=compute_52,code=sm_52"
SM_60="-gencode arch=compute_60,code=sm_60"
SM_61="-gencode arch=compute_61,code=sm_61"
SM_70="-gencode arch=compute_70,code=sm_70"

#9.0 ... does not compile correctly
CUDAversions=( 6.5 7.0 7.5 8.0 9.1 )

for c in "${CUDAversions[@]}"
do

    export CUDA=/opt/cuda-${c}
    
    if [ $(bc <<< "$c < 7.0") -eq 1 ]; then
        # 6.x
    	export GPU_CARDs="$SM_20 $SM_21 $SM_30 $SM_35 $SM_37 $SM_50"
    elif [ $(bc <<< "$c < 8.0") -eq 1 ]; then
        # 7.x
	export GPU_CARDs="$SM_20 $SM_21 $SM_30 $SM_35 $SM_37 $SM_50 $SM_52"
    elif [ $(bc <<< "$c < 9.0") -eq 1 ]; then
        # 8.x
        export GPU_CARDs="$SM_20 $SM_21 $SM_30 $SM_35 $SM_37 $SM_50 $SM_52 $SM_60 $SM_61"
    else 
        # 9.x
        export GPU_CARDs="$SM_30 $SM_35 $SM_37 $SM_50 $SM_52 $SM_60 $SM_61 $SM_70"
    fi	 

    echo "Compiling with $CUDA and compute capabilities: $GPU_CARDs"

    mkdir -p CUDA_$c
    mkdir -p /home/fs0/moisesf/www/Probtrackx_GPU/CUDA_$c

    rm bin/probtrackx2_gpu
    rm -rf /home/fs0/moisesf/www/Probtrackx_GPU/CUDA_$c/*
    rm -rf CUDA_$c/*
    cd ptx2
    rm probtrackx_gpu.o
    rm probtrackx2_gpu
    rm tractography_gpu.o
    rm link_gpu.o
    rm CUDA/*.o
    make install
    cd ..
    cp bin/probtrackx2_gpu /home/fs0/moisesf/www/Probtrackx_GPU/CUDA_$c/
    mydir=`pwd`
    cd /home/fs0/moisesf/www/Probtrackx_GPU/CUDA_$c/ 
    zip -r probtrackx2_gpu.zip .
    cd $mydir
    cp bin/probtrackx2_gpu CUDA_$c/
done


